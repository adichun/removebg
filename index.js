const rmBgFromImageFile = require("remove.bg");
const file = `${__dirname}/assets/img/img-1.jpg`;
const outputFile = `${__dirname}/assets/result/result-1.png`;

rmBgFromImageFile
  .removeBackgroundFromImageFile({
    path: file,
    apiKey: "2Ygos8aarkw69TLBPzB6qeYN",
    size: "full",
    type: "auto",
    scale: "original",
    outputFile,
  })
  .then((result) => {
    console.log(`File saved to ${outputFile}`);
    console.log("result :", result);
    // const base64img = result.base64img;
  })
  .catch((errors) => {
    console.log(JSON.stringify(errors));
  });

console.log("dirname : ", __dirname);
